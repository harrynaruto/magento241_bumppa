<?php
/**
 * @category  Norwood
 * @package   Norwood_Migration
 * @author    Tamar Tchelidze <info@scandiweb.com>
 * @copyright Copyright (c) 2021 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Norwood\Migration\Setup\Patch\Data;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Catalog\Model\Product;
/**
 * Class UpdateShortDescriptionLabel
 * @package Norwood\Migration\Setup\Patch\Data
 *
 * Update Short Description label to Product Highlights
 */
class UpdateShortDescriptionLabel implements DataPatchInterface
{
    /**
     * @var EavSetup
     */
    private $eavSetup;
    private const ATTRIBUTE_DATA = [
        'code' => 'short_description',
        'field' => 'frontend_label',
        'value' => 'Product Highlights'
    ];
    /**
     * UpdateShortDescriptionLabel constructor.
     * @param EavSetup $eavSetup
     */
    public function __construct(
        EavSetup $eavSetup
    ) {
        $this->eavSetup = $eavSetup;
    }
    public function apply()
    {
        $this->eavSetup->updateAttribute(
            Product::ENTITY,
            self::ATTRIBUTE_DATA['code'],
            self::ATTRIBUTE_DATA['field'],
            self::ATTRIBUTE_DATA['value'],
        );
    }
    /**
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }
    /**
     * @return array
     */
    public function getAliases()
    {
        return [];
    }
}