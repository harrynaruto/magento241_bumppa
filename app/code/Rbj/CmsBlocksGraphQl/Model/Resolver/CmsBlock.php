<?php
declare(strict_types=1);

namespace Rbj\CmsBlocksGraphQl\Model\Resolver;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

/**
 * Cms Static Block resolver
 */
class CmsBlock implements ResolverInterface
{
    public function __construct(
        \Magento\Cms\Api\BlockRepositoryInterface $blockRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->blockRepository = $blockRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $blocksData = $this->getBlocksData();
        return $blocksData;
    }

    /**
     * @return array
     * @throws GraphQlNoSuchEntityException
     */
    private function getBlocksData(): array
    {
        try {
            /* filter for all the pages */
            $searchCriteria = $this->searchCriteriaBuilder->addFilter('block_id', 1,'gteq')->create();
            $blocks = $this->blockRepository->getList($searchCriteria)->getItems();
            $cmsBlocks['allBlocks'] = [];
            foreach($blocks as $block) {
                $cmsBlocks['allBlocks'][$block->getId()]['identifier'] = $block->getIdentifier();
                $cmsBlocks['allBlocks'][$block->getId()]['name'] = $block->getTitle();
                $cmsBlocks['allBlocks'][$block->getId()]['content'] = $block->getContent();
            }
        } catch (NoSuchEntityException $e) {
            throw new GraphQlNoSuchEntityException(__($e->getMessage()), $e);
        }
        return $cmsBlocks;
    }
}